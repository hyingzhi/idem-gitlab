===========
idem-gitlab
===========

About
=====

TODO

What is POP?
------------

This project is built with `pop <https://pop.readthedocs.io/>`__, a Python-based
implementation of *Plugin Oriented Programming (POP)*. POP seeks to bring
together concepts and wisdom from the history of computing in new ways to solve
modern computing problems.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop-awesome <https://gitlab.com/saltstack/pop/pop-awesome>`__
* `pop-create <https://gitlab.com/saltstack/pop/pop-create/>`__

Getting Started
===============

Prerequisites
-------------

* Python 3.6+
* git *(if installing from source, or contributing to the project)*

Installation
------------

If wanting to use ``idem-gitlab``, you can do so by either
installing from PyPI or from source.

Install from PyPI
+++++++++++++++++

If package is available via PyPI, include the directions.

.. code-block:: bash

  pip install idem-gitlab

Install from source
+++++++++++++++++++

.. code-block:: bash

   # clone repo
   git clone git@<your-project-path>/idem-gitlab.git
   cd idem-gitlab

   # Setup venv
   python3 -m venv .venv
   source .venv/bin/activate
   pip install -e .

Usage
=====

TODO

Examples
--------

.. code-block:: bash

   # Example CLI commands

Roadmap
=======

Reference the `open issues <https://issues.example.com>`__ for a list of
proposed features (and known issues).

Acknowledgements
================

* `Img Shields <https://shields.io>`__ for making repository badges easy.
