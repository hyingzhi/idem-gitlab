import asyncio
import re
from typing import Any
from typing import Dict
from typing import List


LINK = re.compile(r'rel="next" <(.*)>')


async def paginate(hub, ctx, url: str) -> List[Dict[str, Any]]:
    """
    Paginate items from the given gitlab url
    :param hub:
    :param ctx:
    :param url:
    :return:
    """
    while url:
        ret = await hub.exec.request.json.get(
            ctx,
            url=url,
            params={"per_page": 100, "owned": "true"},
            success_codes=[200],
        )
        if not ret["status"]:
            rate_limit = int(ret["headers"].get("RateLimit-Remaining", 0))
            if not rate_limit:
                # try again, the failure was because of rate limits
                delay = int(ret["headers"].get("Retry-After", 0))
                hub.log.debug(f"Waiting for rate limit to expire: {delay}")
                await asyncio.sleep(delay)
                continue
            else:
                # It was a plain failure
                return
        for result in ret["ret"]:
            yield result
        match = LINK.match(ret["headers"].get("Link", ""))
        if not match:
            return
        url = match.group(1)
