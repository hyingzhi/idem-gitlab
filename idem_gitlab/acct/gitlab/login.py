from typing import Any
from typing import Dict

from dict_tools.data import NamespaceDict


async def gather(hub) -> Dict[str, Any]:
    """
    Get a new access token based on a username and password

    Example:
    .. code-block:: yaml

        gitlab.token:
          profile_name:
            endpoint_url: https://gitlab.com
            username: user@example.com
            password: secret
    """
    sub_profiles = {}
    for profile, ctx in hub.acct.PROFILES.get("gitlab.token", {}).items():
        endpoint_url = ctx["endpoint_url"]
        temp_ctx = NamespaceDict(acct={})
        ret = await hub.exec.request.json.post(
            temp_ctx,
            url=f"{endpoint_url}/oauth/token",
            json={
                "username": ctx["username"],
                "password": ctx["password"],
                "grant_type": "password",
            },
        )

        if not ret["status"]:
            hub.log.warning(f"Unable to authenticate gitlab user: {ctx['username']}")
            continue

        access_token = ret["ret"]["access_token"]
        sub_profiles[profile] = dict(
            endpoint_url=f"{endpoint_url}/api/v4",
            headers={"Authorization": f"Bearer {access_token}"},
        )
    return sub_profiles
